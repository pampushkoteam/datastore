## Описание проекта
 
Spring Boot как RESTful backend + React frontend
 
## Описание API
### Address

http://localhost:8080/api

### Endpoints

* HTTP GET : "localhost:8080/api/humans" getting all people.
* HTTP GET : "localhost:8080/api/humans/{id}" get an human by id.
* HTTP POST : "localhost:8080/api/humans" create a new human.
* HTTP PUT : "localhost:8080/api/humans/{id}" update an existing human.
* HTTP DELETE : "localhost:8080/api/humans/{id}" delete an human by id.

## Конфигурационные файлы
 
## Запуск

./mvnw spring-boot:run