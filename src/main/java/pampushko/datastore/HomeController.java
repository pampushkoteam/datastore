package pampushko.datastore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.invoke.MethodHandles;

/**
 *
 */
@Controller
public class HomeController
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@RequestMapping(value = "/")
	public String index()
	{
		return "index";
	}

}
