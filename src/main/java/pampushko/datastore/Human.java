package pampushko.datastore;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.lang.invoke.MethodHandles;

/**
 *
 */
@Data
@Entity
public class Human
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private @Id @GeneratedValue Long id;
	private String firstName;
	private String lastName;
	private String email;
	
	private Human()
	{
	
	}
	
	public Human(String firstName, String lastName, String email)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
}
