package pampushko.datastore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

/**
 *
 */
@Component
public class DatabaseLoader implements CommandLineRunner
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private final HumanRepository repository;
	
	@Autowired
	public DatabaseLoader(HumanRepository repository)
	{
		this.repository = repository;
	}
	
	@Override
	public void run(String... strings) throws Exception
	{
		this.repository.save(new Human("Alexander", "Petrov", "petrov@gmail.com"));
		this.repository.save(new Human("Ivan", "Ivanov", "ivanov@gmail.com"));
	}
}
